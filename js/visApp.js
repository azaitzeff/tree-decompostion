
	// CONFIGURATION
var VisApp = (function() {

	var VisApp = {};

	var width = parseInt(d3.select('#visChart').style('width'), 10);
	var height = parseInt(d3.select('#visChart').style('height'), 10);

	var linewidth = 3

	var _color = d3.scale.category20();

	var color = function(num){
		var c = _color(num);

		return (c=='#7f7f7f') ? '#f7b6d2': ((c=='#f7b6d2') ? '#de9ed6': c);
	};
	// PUBLIC MEMBERS

	var _nodes;

	var _labels;
	var _graph;

	// INITIALIZE!

	var force = d3.layout.force()
		.charge(-120)
		.linkDistance(50)
		.size([width, height]);

	var drag = force.drag()
	    .on("dragstart", dragstart);

	var svg = d3.select("#visChart")
		.append("svg")
		.attr("id","theGraph")
		.attr("width", width)
		.attr("height", height);




	VisApp.begin = function(inputText) {

	    _graph = JSON.parse(inputText);
	    renderGraph(_graph);
		renderGroupLabels(_graph);

	};

	function renderGraph(graph) {

		force
			.nodes(graph.nodes)
			.links(graph.links)
			.start();

		var link = svg.selectAll(".link")
			.data(graph.links)
			.enter().append("line")
			.attr("class", "link")
			.style("stroke", "#999")
  			.style("stroke-opacity", .6)
			.style("stroke-width", linewidth);

		_nodes = svg.selectAll(".node")
			.data(graph.nodes)
			.enter().append("circle")
			.attr("class", "node")
			.attr("r", 12)
			.on("mouseover", function(d,i) {
				d3.select("#notification").text("Node: "+d.name+", Value: "+d.value+", Number of Groups: " + d.group.length);
				focusLabels(i)

			})
			.on("mouseout", function(d) {
				d3.select("#notification").text("");
				unfocusLabels()
			})
			.on("dblclick", dblclick)
			.style('fill', 'grey')
			.style('stroke', 'black')
			.style("stroke-width",0)
			.classed("fixed", function(d){
				return d.fixed;
			})
			.attr("cx", function(d) { return d.x; })
			.attr("cy", function(d) { return d.y; })
			.call(drag);

		_nodes.append("title")
			.text(function(d) { return d.name; });

		force.on("tick", function() {
			link.attr("x1", function(d) { return d.source.x; })
				.attr("y1", function(d) { return d.source.y; })
				.attr("x2", function(d) { return d.target.x; })
				.attr("y2", function(d) { return d.target.y; });

			_nodes.attr("cx", function(d) { return d.x; })
				.attr("cy", function(d) { return d.y; });
		});
	}

	function renderGroupLabels(graph) {

		var container = d3.select('#group-labels');

		_labels = container.selectAll('div')
			.data(graph.groups)
			.enter()
			.append('div')
			.classed('group-label', true)
			.style('background-color', function(d, i) {
				return color(i);
			})
			.text(function(d) {
				return d.name+': '+'Node: [' + d.main.join(', ') + '], '+'Value: '+d.value;
			});

		_labels.on('mouseover', function(d,i) {
			d.group.sort(function(a, b){return a-b});
			d3.select("#notification").text(d.name+': {' + d.group.join(', ') + '}');
			focusGroup(i,d.main);
		});

		_labels.on('mouseout', function(d) {
			d3.select("#notification").text("");
			unfocusGroups();
		});

		if (graph.multipleEig){
			d3.select("#nodeTitle").style('color','red');
		}

	}

	function focusLabels(nodeIndex) {
		_labels.style('background-color', function(d,i) {

			var focused = contains(d.group, nodeIndex);
			return (focused) ? color(i): 'grey';
		});
	}

	function unfocusLabels() {
		_labels.style('background-color', function(d, i) {
				return color(i);
		});
	}

	function focusGroup(groupIndex,mainNodes) {
		_nodes.style("fill", function(d) {

			var focused = contains(d.group, groupIndex);
			return (focused) ? color(groupIndex): 'grey';
		})
			
			.style("stroke-width", function(d,i){
				var focused = contains(mainNodes, i);
				return (focused) ? 4: 0;
			});
	}

	function unfocusGroups() {
		_nodes.style("fill",'grey')
			.style("stroke-width",0);
	}

	function contains(a, obj) {
		var i = a.length;
		while (i--) {
			 if (a[i] === obj) {
					 return true;
			 }
		}
		return false;
	}

	function dblclick(d) {
	  d3.select(this).classed("fixed", d.fixed = false);
	}

	function dragstart(d) {
	  d3.select(this).classed("fixed", d.fixed = true);
	}

	VisApp.save = function(){
		var graph = fomatter(_graph);
		var word = document.getElementById("text-filename1").value;
		var blob = new Blob([JSON.stringify(graph)], {type: "text/plain;charset=utf-8"});
		saveAs(blob, word+".json");

	}

	function fomatter(real){
		var copy = {};
		copy.multipleEig=real.multipleEig;
		copy.nodes=real.nodes;
		copy.links=[];
		for(var k in real.links){
			copy.links.push({source :parseInt(real.links[k].source.name), 
				target: parseInt(real.links[k].target.name)});
		} ;
		copy.groups=real.groups;
		return copy;

	}

	return VisApp;
});
