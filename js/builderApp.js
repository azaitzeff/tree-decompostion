
  // CONFIGURATION
var BuildApp = (function() {

  var BuildApp = {};


  var width = parseInt(d3.select('#buildChart').style('width'), 10);
  var height = parseInt(d3.select('#buildChart').style('height'), 10);
  var linewidth = 3;

  var fill = d3.scale.category20();

  var linker = false;
  var allFixed = false;
  var nodeson = true;

  var first = null;
  var LINK=2;
  var NODE=1;
  var stack=[];

  var force = d3.layout.force()
      .size([width, height])
      .nodes([{}]) // initialize with a single node
      .linkDistance(50)
      .charge(-120)
      .on("tick", tick);

  //TODO: put the creation code inside of an initialize() function
  var svg = d3.select("#buildChart").append("svg")
      .attr("width", width)
      .attr("height", height)
      .on("mousedown", mousedown);

  svg.append("rect")
      .attr("width", width)
      .attr("height", height);

  var nodes = force.nodes(),
      links = force.links(),
      node = svg.selectAll(".node"),
      link = svg.selectAll(".link");


  restart();

  function mousedown() {
    if (nodeson){
      var point = d3.mouse(this),
        node = {x: point[0], y: point[1]},
        n = nodes.push(node);
      stack.push(NODE);
    }


    restart();
  }

  function tick() {
    link.attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    node.attr("cx", function(d) { return d.x; })
        .attr("cy", function(d) { return d.y; });
  }

  function restart() {
    link = link.data(links);

    link.enter().insert("line", ".node")
        .style("stroke-width", linewidth)
        .attr("class", "link");
    link.exit().remove();

    node = node.data(nodes);

    node.enter().insert("circle", ".cursor")
        .attr("class", "node")
        .attr("r", 12)
        .on("mouseover", function(d,i) {
          d3.select("#notification").text(d.index);
        })
        .on("mouseout", function(d) {
          d3.select("#notification").text("");
        })
        .on("mousedown", function(d){
          if (linker){
            if (first==null){
              first=d;
              node.style('fill', function(t){
                return (t==d)? 'red':'grey';
              });
            }
            else{
              if (first!=d){
                links.push({source: first, target: d});
                stack.push(LINK);
              }
              node.style('fill', function(t){
                return 'grey';
              })
              first=null;
            }
          }
        })
        .classed("fixed", function(d){
          return d.fixed = allFixed;
        })
        .call(force.drag);
        node.exit().remove();

    force.start();
  }

  BuildApp.undo = function(){
    if (stack.length>0){
      var check=stack.pop();
      if (check==NODE){
        nodes.pop();
      } else if (check==LINK){
        links.pop();
      }
      restart();
    }
    
  }

  BuildApp.flip = function(){
    d3.select('#moveButton').style('background','white');
    d3.select('#linkButton').style('background','green');
    d3.select('#newNodesButton').style('background','white');
    nodeson=false;
    linker=true;
    first=null;
    node.style('fill','grey');
    first=null;
  }

  BuildApp.move = function(){
    d3.select('#moveButton').style('background','green');
    d3.select('#linkButton').style('background','white');
    d3.select('#newNodesButton').style('background','white');
    nodeson=false;
    linker=false;
    first=null;
  }

  BuildApp.nodesOn = function(){
    d3.select('#moveButton').style('background','white');
    d3.select('#linkButton').style('background','white');
    d3.select('#newNodesButton').style('background','green');
    nodeson=true;
    linker=false;
    first=null;
    node.style('fill','grey');
    first=null;
  };

  BuildApp.fixed = function(){
    if (allFixed){
      node.classed("fixed", function(d){
        return d.fixed = false;
      });
      d3.select('#fixedButton')
        .attr("value","Force-Directed");
    }
    else{
      node.classed("fixed", function(d){
        return d.fixed = true;
      });
      d3.select('#fixedButton')
        .attr("value","Fixed");
    }
    allFixed=(!allFixed);
    
  };


  BuildApp.algorithm =  function(callback) {
    /*var test='{"nodes":[{"name":"0","group":[2]},{"name":"1","group":[2]},{"name":"2","group":[1, 2]},{"name":"3","group":[0]},{"name":"4","group":[0, 1]}],"links":[{"source":2,"target":0},{"source":2,"target":1},{"source":4,"target":2},{"source":4,"target":3}],"groups":[{"name":"W0","group":[3, 4],"main":[3],"value":-1.57},{"name":"W1","group":[2, 4],"main":[4],"value":-0.76},{"name":"W2","group":[0, 1, 2],"main":[0, 1],"value":0.94}]}';
    callback(test);
    return;*/
    var str = fomatter(links);
    var thegraph ='';
    // var word = document.getElementById("text-filename").value;

    $.post('/pythonscript',{
        data: str
      },
      function(data, status){
          console.log('worked')
          thegraph=data;

          //NOW you can do stuff with thegraph.
          //TODO: parse string into JSON
          callback(thegraph);
      });

  };

  BuildApp.save = function(){
    var str = fomatter(links);
    var word = document.getElementById("text-filename").value;
    var blob = new Blob([str], {type: "text/plain;charset=utf-8"});
    saveAs(blob, word+".txt");

  }

  function fomatter(links){
        var str = "";
        for(var k in links){
            str+="\n"+links[k].source.index+" "+links[k].target.index
        } ;
      return str;

  }

  return BuildApp;
})();

