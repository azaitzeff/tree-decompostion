var myVisApp;
var vis=false;
var num=1;

function runAll(){

    console.log('running the algorithm');

    BuildApp.algorithm(function(graph) {
        switchvis(graph);
    });

}

// Promise pattern

// BuildApp.algorithm().then(function() {

// }).then(function() {

// });
function switchvis(graph){
    d3.select('#buildChart').style('display','none');
    d3.select('#visChart').style('display','block');
    myVisApp = VisApp();
    myVisApp.begin(graph);
    d3.select('#controls1').style('display','none');
    d3.select('#controls2').style('display','block');
    vis=true;
    
}

var openFile1 = function(event) {
    var input = event.target;
    var reader = new FileReader();
    reader.onload = function(){
        var str = reader.result;
        $.post('/pythonscript',{
            data: str
        },
        function(data, status){
          console.log('worked')
          thegraph=data;
          //NOW you can do stuff with thegraph.
          //TODO: parse string into JSON
          switchvis(thegraph);
      });
    };
    reader.readAsText(input.files[0]);
};

var openFile2 = function(event) {
    var input = event.target;
    
    var reader = new FileReader();
    reader.onload = function(){
        var graph = reader.result;
        switchvis(graph)
    };
    reader.readAsText(input.files[0]);
};


function fixed(){
	BuildApp.fixed();
}

function move(){
    BuildApp.move();
}
function nodesOn(){
	BuildApp.nodesOn();
}

function flip(){
	BuildApp.flip();
}

function undo(){
    BuildApp.undo();
}

function save1(){
    BuildApp.save();
}

function save(){
	myVisApp.save();
}

var KEY_BACKTICK=192;
d3.select("body")
    .on("keyup", function() {
        if (d3.event.which==KEY_BACKTICK && vis && document.activeElement.type!='text'){
            try {
                var isFileSaverSupported = !!new Blob();
            } catch (e) {
                alert("blob not supported");
            }

            var html = d3.select("#theGraph")
                .attr("title", "test2")
                .attr("version", 1.1)
                .attr("xmlns", "http://www.w3.org/2000/svg")
                .node().parentNode.innerHTML;

            var blob = new Blob([html], {type: "image/svg+xml"});
            saveAs(blob, document.getElementById("png-filename").value+num+".svg");
            num=num+1;
            }

    });